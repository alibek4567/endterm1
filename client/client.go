package main

import (
	"context"
	"fmt"
	"gitlab.com/alibek4567/awesomeProject/prime"
	"io"

	"google.golang.org/grpc"
	"log"
)

func main(){
	fmt.Println("Hello i'm a client")

	conn,err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil{
		log.Fatalf("could not connect %v",err)
	}
	defer conn.Close()

	c := prime.NewPrimeServiceClient(conn)

	ctx := context.Background()
	request := &prime.PrimeDecompositionRequest{
		Number: &prime.PrimeNumber{
			Number: 120,
		}}

	stream , err := c.PrimeDecomposition(ctx, request)
	if err != nil{
		log.Fatalf("Error while calling Sum rpc %v",err)
	}
	defer stream.CloseSend()

LOOP:
	for {
		res, err:= stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			log.Fatalf("error while reciving from PrimeDecomposition %v", err)
		}
		log.Printf("response from PrimeDecomposition %v \n",res.GetResult())
	}

}
