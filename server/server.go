package main

import (
	"fmt"
	"gitlab.com/alibek4567/awesomeProject/prime"
	"google.golang.org/grpc"
	"io"
	"log"
	"net"
)

type Server struct {
	prime.UnimplementedPrimeServiceServer
}

func (s *Server) ComputeAverage(stream *prime.PrimeService_ComputeAverageServer) error {
	fmt.Println("Received ComputeAverage RPC")

	sum := int32(0)
	count := 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			average := float64(sum) / float64(count)
			stream.SendAndClose(&prime.ComputeAverageResponse{
				Average: average,
			})
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
		}

		sum += req.G
		count++
	}
}

func (s *Server) PrimeDecomposition(req *prime.PrimeDecompositionRequest, stream prime.PrimeService_PrimeDecompositionServer) error {
	number := req.GetNumber().GetNumber()
	k := int32(2)
	for number > 1 {
		if number%k == 0 {
			res := &prime.PrimeDecompositionResponse{Result: k}
			number = number / k
			if err := stream.Send(res); err != nil {
				log.Fatalf("error while sending request %v", err.Error())
			}
		} else {
			k = k + 1
		}
	}
	return nil
}

func main(){
	l,err := net.Listen("tcp","0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen %v",err)
	}

	s := grpc.NewServer()
	prime.RegisterPrimeServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve %v", err)
	}
}

